$(function() {

  // Theme logic begin
  // https://dev.to/ananyaneogi/create-a-dark-light-mode-switch-with-css-variables-34l8

  var currentTheme = localStorage.getItem('theme');
  var toggleSwitch = document.querySelector('.theme-switch input[type="checkbox"]');

  // Update theme if the user has a previously used preference
  if (currentTheme) {
    document.documentElement.setAttribute('data-theme', currentTheme);

    if (currentTheme === 'dark') {
      toggleSwitch.checked = true;
    }
  }

  toggleSwitch.addEventListener('change', toggleTheme, false);

  // Theme logic end

  // Pager logic begin
  var postDisplay = 5;
  var initialUrl = new URL(document.location.href);
  initialUrl = initialUrl.searchParams.get('page');

  var currentUrl = new URL(document.location.href);
  if (currentUrl.searchParams.get('search')) {
    displaySearchResults();
    displaySearchPagerItems(postDisplay);
  }

  // Preserve back button functionality
  $(window).on('popstate', function() {
    location.reload();
  });

  // Search form functionality
  $('#search').submit(function(e) {
    e.preventDefault();
    displaySearchResults();
    displaySearchPagerItems(postDisplay);
  });

  // Add active state the current pager list item
  displayPage(initialUrl ? initialUrl : 1, postDisplay);
  $('.pager__item[data-page=' + (initialUrl ? initialUrl : 1) + ']').addClass('pager__item--active');

  // Pager navigation functionality
  $('.pager__item').click(function() {
    var pagerSelection = $(this).attr('data-page');
    var currentUrl = new URL(document.location.href);
    $('.pager__item').removeClass('pager__item--active');
    $(this).addClass('pager__item--active');
    displayPage(pagerSelection, postDisplay);

    if (history.pushState) {
      currentUrl.searchParams.set('page', pagerSelection);
      updatedUrl = currentUrl.toString();
      window.history.pushState({path:updatedUrl}, '', updatedUrl);
    }
  });

  // Pager logic end

});

function toggleTheme(event) {
  if (event.target.checked) {
    document.documentElement.setAttribute('data-theme', 'dark');
    localStorage.setItem('theme', 'dark');
  }
  else {
    document.documentElement.setAttribute('data-theme', 'light');
    localStorage.setItem('theme', 'light');
  }
}

function displaySearchResults() {
  var currentUrl = new URL(document.location.href);
  var searchTerm = $('.search__input').val() ? $('.search__input').val() : currentUrl.searchParams.get('search');

  // Populate the search field with the current search
  $('#search input').val(searchTerm);
  currentUrl.searchParams.set('search', searchTerm);

  if (searchTerm !== '') {
    $('.post--teaser').hide();
    $('.post--teaser[data-title*=' + searchTerm + ']').show();
    $('.post--teaser[data-tags*=' + searchTerm + ']').show();
  }

  if (history.pushState) {
    currentUrl.searchParams.set('search', searchTerm);
    updatedUrl = currentUrl.toString();
    window.history.pushState({path:updatedUrl}, '', updatedUrl);
  }
}

function displaySearchPagerItems(postDisplay) {
  var postCount = $('.post:visible').length;
  var pageCount = postCount / postDisplay;
  $('.pager').empty();

  for (var i = 0; i < pageCount; i++){
    $('.pager').append('<li class="pager__item" data-page="' + (i + 1) + '">Page ' + (i + 1) + '</li>')
  }
}

function displayPage(pagerSelection, postCount) {
  var postStart = ((pagerSelection - 1) * postCount) + 1;
  var postEnd = postStart + (postCount - 1);

  $('.post--teaser').hide();
  $('.post--teaser:nth-child(n + ' + postStart + '):nth-child(-n + ' + postEnd + ')').show();
}
