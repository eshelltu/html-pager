var gulp = require('gulp'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  clean = require('gulp-clean-css'),
  runsequence = require('run-sequence'),
  prefixer = require('gulp-autoprefixer');

gulp.task('scss', function () {
  gulp.src('styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(prefixer())
    .pipe(clean())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('.'))
});

// Full build task
gulp.task('build', function(done) {
  runsequence('scss', function() {
    done();
  });
});

// All Watch tasks
gulp.task('default', ['build'], function() {
  gulp.watch('*.scss', ['scss']);
});